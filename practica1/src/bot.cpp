#include <iostream>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>

#include "DatosMemCompartida.h"

int main(){

	// Creamos el fichero, el puntero a la memoria y el puntero para la proyeccion

	int file;
	DatosMemCompartida *pMemComp;
	char * proyeccion;

	// Abrimos el fichero
	
	file=open("datosBot.txt", O_RDWR);
	
	// Proyectamos el fichero mediante mmap	
	
	proyeccion=(char*)mmap(NULL,sizeof(*(pMemComp)), PROT_WRITE|PROT_READ,MAP_SHARED,file,0);

	// Cerramos el fichero
	
	close(file);
	
	// Ponemos el puntero para que apunte a la proyeccion
	
	pMemComp=(DatosMemCompartida*)proyeccion;

	//Control raqueta
	
	while(1){
	
		float posRaqueta1;
		
		posRaqueta1=((pMemComp->raqueta1.y2+pMemComp->raqueta1.y1)/2);
		if(posRaqueta1<pMemComp->esfera.centro.y)
				pMemComp->accion1=1;
		else if (posRaqueta1>pMemComp->esfera.centro.y)
				pMemComp->accion1=-1;
		else 
				pMemComp->accion1=0;
        	}

 	// Eliminamos la proyeccion
	
	munmap(proyeccion,sizeof(*(pMemComp)));
}


