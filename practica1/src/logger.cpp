#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

int main( int argc , char* argv[]){

	mkfifo("fifo_practica3",0777);
	int fd=open("fifo_practica3",O_RDONLY);
	if(fd<0)
		{
		   perror("Fallo al abrir la tubería");
		   return(1);
		}
	int aux;
	char buffer[200];
	while(1)
	       {
		   aux=read(fd,buffer,sizeof(buffer));
		   if(aux<0){

			perror("Fallo al leer la tubería");
			return(1);

		   }

		   if(aux==0){

			perror("No hay datos para leer");
			return(1);

		   }
		}
	close(fd);
	unlink("fifo_practica3");
	return 0;
}




		
